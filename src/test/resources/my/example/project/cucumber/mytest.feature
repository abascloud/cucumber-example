Feature: A few tests

// @persistent indicates that created objects should not be deleted automatically
@persistent
Scenario: My first test
	Given I'm logged in as "sy"
	And I open an editor "MY_EDITOR" from table "00:01" with command "NEW" for record ""
	And I set field "such" to "MY_SUCH"
	When I save the current editor
	Then field "nummer" is not empty
	And field "such" has value "MY_SUCH"
	
@persistent
Scenario: My second test with a custom step
	Given I'm logged in as "sy"
	And I open an editor "MY_EDITOR" from table "(Customer):(Customer)" with command "NEW" for record ""
	And I set field "such" to "MY_SUCH"
	And I execute a customed defined step that alters the searchword
	When I save the current editor
	Then field "nummer" is not empty
	And field "such" has value "ALTERED"