Feature: Creating sales orders always calculates the correct sum based on the selected product.

Scenario Outline: Create multiple sales orders and check the sum.
    Given I'm logged in as "sy"
    And I set the variable language to "EN"
	Given I open an editor "MY_SALES_ORDER" from table "03:22" with command "NEW" for record ""
	And I set field "swd" to "<swd>"
	And I set field "customer" to "<customer>"
	And I create a new row at the end of the table
	And I set field "product" to "<product>" in row 1
	And I set field "price" to "<price>" in row 1
	And I set field "unitQty" to "<amount>" in row 1
	When I press button "calcItemVal" in row 1
	Then field "totalNetAmt" has value "<net>"

Examples:
    | swd    | customer | product | price | amount | net      |
    | FIRST  |  70007   |  C00003 | 5000  |  11    | 55000.00 |
    | SECND  |  70008   |  C00002 | 5     |  3     | 15.00    |
    | THIRD  |  70009   |  C00001 | 0     |  20    | 0.00     |



