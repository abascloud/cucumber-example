# Example project for cucumber-jedp

## Setup

### Requirements
* java 1.6 or higher
* eclipse with Plugin cucumber for Natural/agileware or IntelliJ with cucumber Plugin

### Gradle Setup
Add a file 'gradle.properties' to your GRADLE_USER_HOME (Default: "$USER_HOME/.gradle/gradle.properties") with the following content:

```
#If you use a proxy add it here
http.proxyHost=<Your Proxy Host>
http.proxyPort=<Your Proxy Port>
https.proxyHost=<Your Proxy Host>
https.proxyPort=<Your Proxy Port>

# abas Nexus
nexusSnapshotURL=https://registry.abas.sh/repository/abas.snapshots
nexusReleaseURL=https://registry.abas.sh/repository/abas.releases
nexusUser=<NexusUsername>
nexusPassword=<NexusPassword>
```
Username and password are the extranet-Logins.

Depending on your system and or proxy configuration you might also need to add the proxy settings to your JAVA execution environment. That can be done either via:
```
-Djava.net.useSystemProxies=true
```
or
```
JAVA_OPTS="-Dhttp.proxyHost=<Your Proxy Host> -Dhttp.proxyPort=<Your Proxy Port>"
```

### Eclipse
You can import the project into eclipse if you have the gradle plugin installed.
If you don't have it installed you can execute the following command in the project directory to create all necessary eclipse files:
```
./gradlew eclipse
```

You can get the plugin for syntax highlightning and auto completion from the ecliipse Marketplace. It's called Natural/agileware.

## Usage
The project is only meant to be an example and a stepping stone for your own tests. You can browse the .feature-files from src/test/resources to get to know some of the steps.
You can easily add new steps in the provided 'MyCustomSteps'-Java class and they will be available for the execution of your tests.

Before you can run the tests you have to provide the connection parameters first. You can do that in the included setup.properties under src/test/resources/my/example/project . To run them, you have to execute the RunCukes-Java class in your IDE as a JUnit-Test.
As an alternative you can also execute
```
./gradlew test
```
